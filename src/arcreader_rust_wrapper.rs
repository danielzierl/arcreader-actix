use serde::Serialize;

extern crate libc;
pub fn read_wrp(
    root_dir: String,
    dt_start: String,
    dt_end: String,
    signals: &Vec<i32>,
) -> Vec<ExportedRow> {
    let len_exported_rows: usize = 0;
    let len_ptr: *const usize = &len_exported_rows;
    let mut out_vec: Vec<ExportedRow> = vec![];
    unsafe {
        let exported_rows = read(
            root_dir.as_ptr(),
            dt_start.as_ptr(),
            dt_end.as_ptr(),
            signals.as_ptr(),
            signals.len(),
            len_ptr,
        );
        for i in 0..len_exported_rows {
            out_vec.push((*exported_rows.wrapping_add(i)).to_exported_row());
        }
        if exported_rows == 0 as *const ExportedRowCRepr {
            return vec![];
        }
        libc::free(exported_rows as *mut libc::c_void);
    };
    return out_vec;
}
extern "C" {
    fn read(
        root_dir: *const u8,
        dt_start: *const u8,
        dt_end: *const u8,
        signals: *const i32,
        signals_len: usize,
        size_of_exported_rows: *const usize,
    ) -> *const ExportedRowCRepr;
}
#[derive(Debug, Serialize)]
pub struct ExportedRow {
    pub year: i32,
    pub month: i32,
    pub day: i32,
    pub hour: i32,
    pub min: i32,
    pub sec: i32,
    pub nano_sec: i32,
    pub value_type: i32,
    pub id: i32,
}
impl ExportedRowCRepr {
    pub fn to_exported_row(&self) -> ExportedRow {
        ExportedRow {
            year: self.year,
            month: self.month,
            day: self.day,
            hour: self.hour,
            min: self.min,
            sec: self.sec,
            nano_sec: self.nano_sec,
            value_type: self.value_type,
            id: self.id,
        }
    }
}
#[repr(C)]
pub struct ExportedRowCRepr {
    year: i32,
    month: i32,
    day: i32,
    hour: i32,
    min: i32,
    sec: i32,
    nano_sec: i32,
    value_type: i32,
    value: Exported_row_u_value,
    id: i32,
}

#[repr(C)]
pub union Exported_row_u_value {
    value_int: i32,
    value_str: *const u8,
    value_bool: i32,
    value_double: f64,
}
// impl std::fmt::Debug for Exported_row_u_value {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         unsafe {
//             f.debug_struct("Exported_row_u_value")
//                 .field("value_int", &self.value_int)
//                 .field("value_str", &self.value_str)
//                 .field("value_bool", &self.value_bool)
//                 .field("value_double", &self.value_double)
//                 .finish()
//         }
//     }
// }
