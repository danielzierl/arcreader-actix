mod arcreader_rust_wrapper;
use actix_web::{post, web, App, HttpResponse, HttpServer, Responder};
use arcreader_rust_wrapper::read_wrp;
use serde::{Deserialize, Serialize};
//
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    match HttpServer::new(|| App::new().service(get_rows)).bind("0.0.0.0:8080") {
        Ok(server) => server,
        Err(e) => {
            eprintln!("Failed to start server: {}", e);
            std::process::exit(1);
        }
    }
    .run()
    .await
}

#[derive(Deserialize, Serialize, Debug)]
struct PostParams {
    root_path: String,
    dt_start: String,
    dt_end: String,
    signals: Vec<i32>,
}

#[post("/")]
async fn get_rows(post_body: web::Json<PostParams>) -> impl Responder {
    println!("{:?}", post_body);
    if post_body.root_path.is_empty() {
        return HttpResponse::Ok()
            .body("Empty root path (did you forget to set the root_path query parameter?)");
    }
    if post_body.dt_start.is_empty() {
        return HttpResponse::Ok()
            .body("Empty starting date (did you forget to set the dt_start query parameter?)");
    }
    if post_body.dt_end.is_empty() {
        return HttpResponse::Ok()
            .body("Empty ending date (did you forget to set the dt_end query parameter?)");
    }
    if post_body.signals.is_empty() {
        return HttpResponse::Ok()
            .body("Empty signals (did you forget to set the signals query parameter?)");
    }
    let root_path = post_body.root_path.to_string();
    let dt_start = post_body.dt_start.to_string();
    let dt_end = post_body.dt_end.to_string();
    let signals = &post_body.signals;

    let exported_rows = read_wrp(root_path, dt_start, dt_end, signals);
    HttpResponse::Ok()
        .content_type("application/json")
        .json(exported_rows)
    // println!("{:?}", exported_rows);
    // format!("{:?}", exported_rows)
}
// fn main() {
//     let root_path = String::from("/home/pollux/Documents/archives-sample");
//     let dt_start = String::from("2018-01-01 00:00:00");
//     let dt_end = String::from("2024-01-02 00:00:00");
//     let signals = vec![206848];
//     let exported_rows = read_wrp(root_path, dt_start, dt_end, signals);
//     // println!("{:?}", exported_rows);
//     let str_out = format!("{:?}", exported_rows);
//     println!("{}", str_out);
// }
